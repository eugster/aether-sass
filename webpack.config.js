/*
 * SPDX-FileCopyrightText: 2020 (c) Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('build/')
    // public path used by the web server to access the output path
    .setPublicPath('/aether-devel')
    // only needed for CDN's or sub-directory deploy
    .setManifestKeyPrefix('aether-devel/')

    // shared between all websites
    .addEntry('bootstrap', './js/bootstrap.js')

    // website specific code
    .addEntry('charjs', './js/chart.js')
    .addEntry('piwikbanner', './js/piwikbanner.js')

    .addStyleEntry('aether-mediawiki', './css/aether-mediawiki.scss')
    .addStyleEntry('aether-sidebar', './css/aether-sidebar.scss')

    // kde.org subpage
    .addEntry('kde-org/announcement', './js/announcement.js')
    .addEntry('kde-org/applications', './js/applications.js')
    .addStyleEntry('kde-org/index', './css/kde-org/index.scss')
    .addStyleEntry('kde-org/products/kirigami', './css/kde-org/kirigami.scss')
    .addStyleEntry('kde-org/products', './css/kde-org/products.scss')
    .addStyleEntry('kde-org/plasma-desktop', './css/kde-org/plasma-desktop.scss')
    .addStyleEntry('kde-org/clipart', './css/kde-org/clipart.scss')
    .addStyleEntry('kde-org/frameworks', './css/kde-org/frameworks.scss')
    .addStyleEntry('kde-org/releaseAnnouncement', './css/kde-org/releaseAnnouncement.scss')

    // .enableSingleRuntimeChunk()
    .disableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())

    .configureFilenames({
        images: 'images/[name].[ext]',
        fonts: 'fonts/[name].[ext]'
    })

    .autoProvidejQuery()

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()
;

if (process.env.VERSIONING === "true") {
    Encore.enableVersioning()
        .setOutputPath('build/version')
        .setPublicPath('/aether-devel/version')
        .setManifestKeyPrefix('aether-devel/version/')

        .configureFilenames({
            images: 'images/[name].[hash:8].[ext]',
            fonts: 'fonts/[name].[hash:8].[ext]'
        });
}


if (process.env.LOCAL === "true") {
    Encore.setOutputPath('build/version')
        .setPublicPath('/build/version')
        .setManifestKeyPrefix('build/version/')
        .configureFilenames({
            images: 'images/[name].[hash:8].[ext]',
            fonts: 'fonts/[name].[hash:8].[ext]'
        });

    if (process.env.VERSIONING === "true") {
        Encore.enableVersioning();
    }
}

module.exports = Encore.getWebpackConfig();
